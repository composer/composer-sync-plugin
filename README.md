Installation
============

- Edit ~/.composer/composer.json :
```json
    "repositories": {
        "triotech": {
            "type": "composer",
            "url": "https://packages.triotech.fr"
        }
    }
```

- Execute shell command :
```bash
    composer global require triotech/composer-sync-plugin dev-master@dev
```