<?php

namespace TRIOTECH\Composer;

use Composer\Plugin\Capability\CommandProvider;

class SyncCommandProvider implements CommandProvider
{
    /** @var SyncCommand */
    private $syncCommand;

    /** @inheritdoc */
    public function __construct(array $args = [])
    {
        $this->syncCommand = new SyncCommand($args['composer']->getPackage(), $args['composer']->getLocker(), $args['composer']->getConfig());
    }

    /** @inheritdoc */
    public function getCommands()
    {
        return [
            $this->syncCommand,
        ];
    }
}
