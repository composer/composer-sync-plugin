<?php

namespace TRIOTECH\Composer;

use Composer\Command\BaseCommand;
use Composer\Config;
use Composer\Package\BasePackage;
use Composer\Package\Package;
use Composer\Package\Locker;
use Composer\Repository\PlatformRepository;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SyncCommand extends BaseCommand
{
    /** @var BasePackage */
    protected $package;
    /** @var Locker */
    protected $locker;
    /** @var Config */
    protected $config;

    /**
     * @param BasePackage $package
     * @param Locker $locker
     * @param Config $config
     */
    public function __construct(BasePackage $package, Locker $locker, Config $config)
    {
        parent::__construct('sync');

        $this->package = $package;
        $this->locker = $locker;
        $this->config = $config;
    }

    /** @inheritdoc */
    protected function configure()
    {
        $this->setDescription('Synchronizes the composer.lock file with current installed dependencies versions.');
    }

    /** @inheritdoc */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Synchronizing git+dev-packages from composer.lock file...');
        $packages = $this->locker->getLockedRepository()->getPackages();
        $vendor = $this->config->get('vendor-dir');
        $now = new \DateTime();
        $updates = 0;

        foreach ($packages as $idx => $package) {
            if (strpos($package->getPrettyVersion(), 'dev') !== false && $package->getSourceType() === 'git') {
                $files = [];
                $path = $vendor . DIRECTORY_SEPARATOR . $package->getName();

                if (!file_exists($path)) {
                    continue;
                }

                $status = $this->executeGitCommand('status . --porcelain', $path, $code, $files);

                if ($code !== 0) {
                    continue;
                }

                if (!empty($status)) {
                    $files = implode(PHP_EOL, $files);
                    $output->writeln(sprintf("- Working tree of %s is not clean:\n%s", $path, $files));
                    continue;
                }

                $sha = $this->executeGitCommand('rev-parse HEAD', $path);
                $ref = $package->getSourceReference();

                if ($sha !== $ref) {
                    $updates++;
                    $package->setSourceReference($sha);
                    if ($package instanceof Package) {
                        $package->setReleaseDate($now);
                    }
                    $output->writeln(sprintf('- Updated package %s to %s', $package->getName(), $sha));
                }
            }
        }

        if ($this->updatePackages($packages)) {
            exec('composer install');
            $output->writeln(sprintf('Done ! (%d package(s) updated)', $updates));
        } else {
            $output->writeln('Nothing to update.');
        }
    }

    /**
     * @param string $command
     * @param string $path
     * @param int $returnCode
     * @param array $output
     *
     * @return string
     */
    public function executeGitCommand($command, $path = '', &$returnCode = 0, &$output = [])
    {
        return trim(exec("git -C $path $command", $output, $returnCode));
    }

    /**
     * @param  array $links
     *
     * @return array
     */
    protected function extractPlatformRequirements($links)
    {
        $platformReqs = [];
        foreach ($links as $link) {
            if (preg_match(PlatformRepository::PLATFORM_PACKAGE_REGEX, $link->getTarget())) {
                $platformReqs[$link->getTarget()] = $link->getPrettyConstraint();
            }
        }

        return $platformReqs;
    }

    /**
     * @param array $packages
     *
     * @return bool
     */
    protected function updatePackages($packages)
    {
        $devPackages = array_slice($this->locker->getLockedRepository(true)->getPackages(), count($packages));
        $platforms = $this->extractPlatformRequirements($this->package->getRequires());
        $devPlatforms = $this->extractPlatformRequirements($this->package->getDevRequires());
        $minStability = $this->locker->getMinimumStability();
        $stabilityFlags = $this->locker->getStabilityFlags();
        $preferStable = $this->locker->getPreferStable();
        $preferLowest = $this->locker->getPreferLowest();
        $platformOverrides = $this->locker->getPlatformOverrides();

        $aliases = array_reduce($this->locker->getAliases(), function ($carry, $alias) {
            $package = $alias['package'];
            $version = $alias['version'];

            if (!array_key_exists($package, $carry)) {
                $carry[$package] = [];
            }

            $carry[$package][$version] = $alias;

            return $carry;
        }, []);

        return $this->locker->setLockData($packages, $devPackages, $platforms, $devPlatforms, $aliases, $minStability, $stabilityFlags, $preferStable, $preferLowest, $platformOverrides);
    }
}
