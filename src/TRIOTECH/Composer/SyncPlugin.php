<?php

namespace TRIOTECH\Composer;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\Capability\CommandProvider;
use Composer\Plugin\Capable;
use Composer\Plugin\PluginInterface;

class SyncPlugin implements PluginInterface, Capable
{
    /** @inheritdoc */
    public function activate(Composer $composer, IOInterface $io)
    {
    }

    /** @inheritdoc */
    public function getCapabilities()
    {
        return [
            CommandProvider::class => SyncCommandProvider::class,
        ];
    }
}
